-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 15, 2016 at 12:55 PM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tuv`
--

--
-- Dumping data for table `wae05_categories`
--

INSERT INTO `wae05_categories` (`id`, `name`, `parent_id`) VALUES
(1, 'Hauptspeisen', NULL),
(2, 'Vorspeisen', NULL),
(3, 'Nachspeisen', NULL),
(4, 'Suppen', 2),
(5, 'Salate', 2),
(6, 'Vegan', 1),
(7, 'Vegetarisch', 1),
(8, 'Fischgerichte', 1),
(9, 'Fleischgerichte', 1),
(14, 'kalt', 3),
(15, 'warm', 3),
(16, 'Backwaren', NULL),
(17, 'Kuchen', 16),
(18, 'Kekse', 16),
(19, 'Antipasti', 2);

--
-- Dumping data for table `wae05_difficulty`
--

INSERT INTO `wae05_difficulty` (`id`, `description`) VALUES
(1, 'Anf&aumlnger'),
(4, 'Experte'),
(3, 'Fortgeschritten'),
(2, 'Hobbykoch');

--
-- Dumping data for table `wae05_users`
--

INSERT INTO `wae05_users` (`id`, `username`, `password`) VALUES
(1, 'Simon', 'test123'),
(2, 'Sylvia', 'test123'),
(3, 'Fabian', 'test123');

--
-- Dumping data for table `wae05_recipes`
--

INSERT INTO `wae05_recipes` (`id`, `category_id`, `title`, `content`, `create_time`, `user_id`, `duration`, `difficulty_id`) VALUES
(3, 7, 'Veggi Teller mit Ei', '<p><img src="https://media.holidaycheck.com/data/urlaubsbilder/images/146/1156777966.jpg" style="width:450px" /></p>\r\n\r\n<h3>Zutaten</h3>\r\n\r\n<ul>\r\n	<li>Gem&uuml;se</li>\r\n	<li>Ei</li>\r\n</ul>\r\n\r\n<h3>Zubereitung</h3>\r\n\r\n<ol>\r\n	<li>Gem&uuml;se anbraten</li>\r\n	<li>Ei anbraten</li>\r\n	<li>Ei auf Gem&uuml;se geben</li>\r\n	<li>Fertig</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n', '2016-01-15 09:54:02', 1, 15, 3),
(4, 8, 'Kabeljau mit frischem GemÃ¼se', '<h3>Hintergrundwissen</h3>\r\n\r\n<p>Der <strong>(Atlantische) Kabeljau</strong> oder <strong>Dorsch</strong> (<em>Gadus morhua</em>) ist ein Meeresfisch, der in Teilen des <a href="https://de.wikipedia.org/wiki/Nordatlantik">Nordatlantiks</a> und des <a href="https://de.wikipedia.org/wiki/Nordpolarmeer">Nordpolarmeers</a> verbreitet ist. Als &bdquo;Dorsch&ldquo; bezeichnet man die in der <a href="https://de.wikipedia.org/wiki/Ostsee">Ostsee</a> lebende Population. In <a href="https://de.wikipedia.org/wiki/Norwegen">Norwegen</a>, <a href="https://de.wikipedia.org/wiki/D%C3%A4nemark">D&auml;nemark</a> und <a href="https://de.wikipedia.org/wiki/Schweden">Schweden</a> hei&szlig;t er <em>Torsk</em>, der arktische Kabeljau wird in Norwegen <em>Skrei</em> genannt. In den <a href="https://de.wikipedia.org/wiki/Niederlande">Niederlanden</a> wird er <em>Kabeljauw</em>, in Polen <em>Dorsz</em>, in englischsprachigen L&auml;ndern <em>Cod</em>, in Frankreich <em>Cabillaud</em> bzw. <em>Morue</em> und in Russland <em>Treska</em> genannt. Der Kabeljau geh&ouml;rt zu den wichtigsten <a href="https://de.wikipedia.org/wiki/Speisefisch">Speisefischen</a>, ist von gro&szlig;er <a href="https://de.wikipedia.org/wiki/Fischerei">fischereiwirtschaftlicher</a> Bedeutung und inzwischen durch <a href="https://de.wikipedia.org/wiki/%C3%9Cberfischung">&Uuml;berfischung</a> gef&auml;hrdet.</p>\r\n\r\n<h3>Zutaten</h3>\r\n\r\n<ul>\r\n	<li>Kabeljau</li>\r\n	<li>Gem&uuml;se</li>\r\n	<li>Frische Kr&auml;uter</li>\r\n</ul>\r\n\r\n<h3>Zubereitung</h3>\r\n\r\n<p>Den Kabeljau sauber waschen, anbraten und genie&szlig;en!</p>\r\n\r\n<p><img src="http://www.cinenord.lu/wp-content/uploads/2015/02/Fischgericht-Restaurant-Kretzergrund.jpg" style="width:400px" /></p>\r\n', '2016-01-15 09:54:32', 1, 40, 4),
(5, 3, 'Ingwer-Creme-brulee', '<h3>Zutaten:</h3>\r\n\r\n<ul>\r\n	<li>3 Himbeeren</li>\r\n	<li>1 Stk Ingwer</li>\r\n	<li>500g Zucker</li>\r\n	<li>1 Bunsenbrenner</li>\r\n</ul>\r\n\r\n<h3>Zubereitung</h3>\r\n\r\n<p>Creme-brulee klassisch zubereiten und mit Bunsenbrenner karamellisieren!</p>\r\n\r\n<p><img src="http://www.fabalista.com/fileadmin/redaktion/fabalista/Artikelbilder/Ingwer-creme-brulee-627.jpg" style="width:500px" /></p>\r\n\r\n<h3>Tipp</h3>\r\n\r\n<p>Den Bunsenbrenner vorher mit Gas f&uuml;llen</p>\r\n', '2016-01-15 09:55:16', 2, 28, 2),
(6, 4, 'Tomatencremesuppe', '<p><img alt="" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/Tomato_soup.jpg/320px-Tomato_soup.jpg" style="height:240px; width:320px" /></p>\r\n\r\n<h3>Definition</h3>\r\n\r\n<p><strong>Tomatensuppe</strong> bezeichnet im Allgemeinen eine Suppe aus <a href="https://de.wikipedia.org/wiki/Tomate">Tomaten</a> verschiedener, aber doch meistens roter, nicht unbedingt der gleichen Sorte angeh&ouml;renden Tomaten. Die Suppe kann je nach regionaler Gegebenheit mit <a href="https://de.wikipedia.org/wiki/Karotte">M&ouml;hren</a> oder anderem Gem&uuml;se geschmacklich erg&auml;nzt werden.</p>\r\n\r\n<h3>Zutaten</h3>\r\n\r\n<ul>\r\n	<li>Tomaten</li>\r\n	<li>Wasser</li>\r\n	<li>Salz</li>\r\n	<li>Pfeffer</li>\r\n</ul>\r\n\r\n<h3>Zubereitung</h3>\r\n\r\n<ol>\r\n	<li>Wasser in einem Topf zum kochen bringen</li>\r\n	<li>Tomaten dazugeben</li>\r\n	<li>Ca. eine halbe Stunde k&ouml;cheln lassen</li>\r\n	<li>Mit Salz und Pfeffer abschmecken</li>\r\n	<li>Genie&szlig;en</li>\r\n</ol>\r\n', '2016-01-15 09:45:58', 3, 45, 1),
(7, 5, 'Caesar Salad', '<p><strong>Caesar Salad</strong> ist ein international bekannter <a href="https://de.wikipedia.org/wiki/Salat_%28Speise%29">Salat</a> der <a href="https://de.wikipedia.org/wiki/US-amerikanische_K%C3%BCche">US-amerikanischen K&uuml;che</a>, der dem <a href="https://de.wikipedia.org/wiki/Italo-Amerikaner">Italo-Amerikaner</a> <a href="https://de.wikipedia.org/wiki/Cesare_Cardini">Cesare Cardini</a> zugeschrieben wird. Andere, meist Cardinis Mitarbeiter, machten ihm erfolglos den Ruhm f&uuml;r die Kreation streitig. So versuchte sein Bruder Alex den Caesar Salad unter den Namen &bdquo;Aviator&#39;s Salad&ldquo; als eigene Kreation zu etablieren.</p>\r\n\r\n<ul>\r\n	<li>Toller Salat</li>\r\n	<li>Noch mehr toller Salat</li>\r\n	<li>Bisschen gesunder Salat</li>\r\n	<li>H&uuml;hnchen</li>\r\n	<li>Dressing</li>\r\n</ul>\r\n\r\n<p><img alt="" src="https://upload.wikimedia.org/wikipedia/commons/a/a7/CaesarSalad3.jpg" style="height:205px; width:304px" /></p>\r\n', '2016-01-15 02:41:07', 3, 35, 3),
(8, 1, 'Tiroler Flammkuchen', '<p><img alt="" src="http://images.ichkoche.at/data/image/variations/496x384/4/default-img-38737.jpg" style="height:384px; width:496px" /></p>\r\n\r\n<p>&nbsp;\r\n<h3>Zutaten</h3>\r\n</p>\r\n\r\n<p>Portionen: 4</p>\r\n\r\n<ul>\r\n</ul>\r\n\r\n<p>F&uuml;r den Germteig:</p>\r\n\r\n<ul>\r\n	<li>500 g Mehl</li>\r\n	<li>20 g Germ (1/2 W&uuml;rfel)</li>\r\n	<li>3 EL Oliven&ouml;l</li>\r\n	<li>1 TL Salz</li>\r\n	<li>250 ml Wasser (lauwarm, ca.)</li>\r\n</ul>\r\n\r\n<p>F&uuml;r den Belag:</p>\r\n\r\n<ul>\r\n	<li>200 g HANDL TYROL Feine Speckw&uuml;rfel</li>\r\n	<li>250 g Zwiebel (rot)</li>\r\n	<li>2 EL Butter</li>\r\n	<li>200 g Cr&egrave;me fra&icirc;che</li>\r\n	<li>Salz</li>\r\n	<li>Pfeffer</li>\r\n	<li>Schnittlauch (zum Bestreuen)</li>\r\n</ul>\r\n\r\n<h3>Zubereitung</h3>\r\n\r\n<p>F&uuml;r den <strong>Tiroler Flammkuchen </strong>zun&auml;chst&nbsp;Mehl mit fein zerbr&ouml;ckeltem Germ, Oliven&ouml;l, Salz und lauwarmem Wasser in eine Sch&uuml;ssel geben. Mit der K&uuml;chenmaschine oder mit den H&auml;nden zu einem sch&ouml;nen Teig verkneten. Teig abgedeckt an einem warmen Ort gehen lassen.</p>\r\n\r\n<p>Zwiebel sch&auml;len und in d&uuml;nne Ringe schneiden. Diese dann mit Butter in einer Pfanne einige Minuten anbraten, bis sie Farbe bekommen.</p>\r\n\r\n<p>Cr&egrave;me fra&icirc;che mit Salz und Pfeffer verr&uuml;hren.</p>\r\n\r\n<p>Den Ofen auf 200 &deg;C vorheizen. Den Germteig teilen und zu einen d&uuml;nnen Fladen ausrollen. Auf ein mit Backpapier belegtes Blech legen und jeden Fladen mit Cr&egrave;me fra&icirc;che bestreichen. Mit Zwiebel und Feinen Schinkenspeckstreifen &nbsp;belegen.</p>\r\n\r\n<p>Die <strong>Tiroler Flammkuchen </strong>im Backofen etwa 10 Minuten knusprig backen. Danach mit ein wenig Schnittlauch bestreuen und frisch servieren.</p>\r\n\r\n<h3>Tipp</h3>\r\n\r\n<p>Probieren Sie verschiedene Salate als Beilage zum&nbsp;<strong>Tiroler Flammkuchen</strong>.</p>\r\n', '2016-01-15 10:16:37', 1, 45, 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
