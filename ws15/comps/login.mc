<%method head>
</%method>
<%init>
my $form_data = delete($m->session->{form_data});
</%init>
<div class="container">
% $.FillInForm($form_data) {{
    <form class="form-signin" id="loginform" action="session/login" method="post">
        <h3 class="form-signin-heading">Bitte loggen Sie sich ein</h3>
        <input class="input-block-level" placeholder="Username" type="text" name="username" required="required">
        <input class="input-block-level" placeholder="Passwort" type="password" name="password" required="required">
        <button class="btn btn-large btn-primary" type="submit" value="Publish">Sign in</button>
    </form>
% }}
</div>