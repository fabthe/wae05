<%class>
  has 'id' => (default => '-1');
</%class>
<%method head>
</%method>
<%init>
    my $query = "SELECT r.id AS id, r.title AS title, r.content AS content, ";
	$query .=   "       r.create_time AS create_time, r.duration AS duration, ";
	$query .=   "       u.username AS username, c.name AS category_name, ";
	$query .=   "       d.description AS difficulty ";
	$query .=   "FROM wae05_recipes r ";
	$query .=   "LEFT JOIN wae05_users u ON r.user_id = u.id ";
    $query .=   "LEFT JOIN wae05_categories c ON r.category_id = c.id ";
    $query .=   "LEFT JOIN wae05_difficulty d ON r.difficulty_id = d.id ";
	$query .=   "WHERE r.id = ?";

    my $dbh = Ws15::DBI->dbh();
    my $sth = $dbh->prepare($query);
    $sth->execute($.id);

    my $recipe;
    $recipe = $sth->fetchrow_hashref;
</%init>

<div class="container">
    <& template/recipe.mi, r => $recipe &>
</div>