<%class>
    has 'id';
    has 'Delete';
</%class>
<%method head>
</%method>
<%init>
my $dbh = Ws15::DBI->dbh();
my $msg = "";

my $uid = $m->session->{'userid'};
if (! ($uid > 0)) {
    $m->redirect("index");
}

if (!(defined($.id))) {
    $m->redirect("index");
}

my $sqlR = $dbh->prepare("SELECT user_id, title FROM wae05_recipes WHERE id = ?");
$sqlR->execute($.id);
if ($sqlR->rows() == 0) {
    $m->redirect("index");
}

my $row = $sqlR->fetchrow_hashref;
if (!($row->{user_id} eq $uid)) {
    $m->redirect("index");
}

if ($.Delete) {
    my $sqlD = $dbh->prepare("DELETE FROM wae05_recipes WHERE id = ?");
    $sqlD->execute($.id);
    $msg = "Das Rezept wurde erfolgreich gelöscht. <u><a href='index'>Zur Startseite.</a></u>";
}
</%init>

<div class="container">
    <h2>Rezept löschen</h2>
% if (length($msg) == 0) {
    <p>Wollen Sie Ihr Rezept wirklich löschen?</p>
    <form name="deleteform" method="post" role="form">
        <input style="float:left" type="submit" name="Delete" class="btn btn-danger" value="Ja, Löschen" /> &nbsp;
        <span style="margin: 7 7 7 7; display:inline-block">
            <a href="index">Abbrechen</a>
        </span>
        <br />
        <br />
        <br />
    </form>
% } else {
    <div class="alert alert-info">
        <strong>Info:</strong> <% $msg %>
    </div>
% }


</div>