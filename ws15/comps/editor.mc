<%class>
    has 'id';
    has 'title';
    has 'textarea' => (default => "<font face=Verdana>bitte hier den Text eingeben.\n</font>\n");
    has 'duration';
    has 'difficulty';
    has 'category';
    has 'Save';
    has 'Delete';
</%class>

<%method head>
    <script src="static/js/ckeditor/ckeditor.js"></script>
</%method>

<%init>

$.maintitle($.maintitle.' | Editieren');

my $uid = $m->session->{'userid'};
if (! ($uid > 0)) {
    $m->redirect("index");
}

use Data::Dumper;
use HTML::Entities;

my $dbh = Ws15::DBI->dbh();
my $msg = "";

my $sqlC = $dbh->prepare("SELECT * FROM wae05_categories");
$sqlC->execute();
my @categories;
while (my $hr = $sqlC->fetchrow_hashref) {
	push(@categories, $hr);
}

my $sqlD = $dbh->prepare("SELECT * FROM wae05_difficulty");
$sqlD->execute();
my @difficulties;
while (my $hr = $sqlD->fetchrow_hashref() ) {
	push(@difficulties, $hr);
}

if ($.Save && not defined($.id)) {
    # Insert new recipe
    my $query = "INSERT INTO wae05_recipes ";
    $query .=   "(category_id, title, content, create_time, user_id, duration, difficulty_id) ";
    $query .=   "VALUES(?, ?, ?, NOW(), ?, ?, ?)";
    my $sqlRI = $dbh->prepare($query);
    $sqlRI->execute($.category, ($.title), ($.textarea), $uid, $.duration, $.difficulty);

    $msg = "Rezept wurde erfolgreich gespeichert. <u><a href='recipe?id=".$sqlRI->{mysql_insertid}."'>Zum Rezept.</a></u>";

    $.category(0);
    $.title("");
    $.textarea("<font face=Verdana>bitte hier den Text eingeben.\n</font>\n");
    $.duration("");
    $.difficulty("");
}
elsif ($.Save && defined($.id)) {
    # Update
    # Check for permissions
    my $checkSql = $dbh->prepare("SELECT * FROM wae05_recipes WHERE user_id = ?");
    $checkSql->execute($uid);
    my $row = $checkSql->fetchrow_hashref;

    #print $uid;
    #print $row; # ->{user_id};

    if ($checkSql->rows() == 0 || !($row->{user_id} eq $uid)) {
        $m->redirect("editor");
    }

    my $query = "UPDATE wae05_recipes SET ";
    $query .=   "category_id = ?, title = ?, content = ?, create_time = NOW(), duration = ?, difficulty_id = ? ";
    $query .=   "WHERE id = ?";
    my $sqlU = $dbh->prepare($query);
    $sqlU->execute($.category, ($.title), ($.textarea), $.duration, $.difficulty, $.id);

    if ($sqlU->rows() > 0) {
        $msg = "Rezept wurde erfolgreich upgedated. <u><a href='recipe?id=".($.id)."'>Zum Rezept.</a></u>";
    }
}
elsif ($.Delete && defined($.id)) {
    $m->redirect("delete?id=".$.id);
}
elsif ($.id > 0) {
    my $sqlR = $dbh->prepare("SELECT title, content, duration, category_id, difficulty_id, user_id FROM wae05_recipes WHERE id = ?");
    $sqlR->execute($.id);
    if ($sqlR->rows() == 0) {
        $m->redirect("editor");
    }
    my $row = $sqlR->fetchrow_hashref;
    if (!($row->{user_id} eq $uid)) {
        $m->redirect("editor");
    }

    $.title($row->{title});
    $.textarea(($row->{content}));
    $.difficulty($row->{difficulty_id});
    $.category($row->{category_id});
    $.duration($row->{duration});
}
</%init>



<div class="container">
    <h2>Neues Rezept hinzufügen</h2>

% if (length($msg) > 0) {
    <div class="alert alert-info">
        <strong>Info:</strong> <% $msg %>
    </div>
% }

    <form name="editform" method="post" role="form">
        <table width="100%" cellspacing="1" cellpadding="4" border="0">
        <colgroup>
            <col align="right" valign="top" width="150px">
            <col align="left">
        </colgroup>
        <tr>
            <td>Rezeptname:</td>
            <td><input type="text" name="title" value="<% $.title %>" style="width: 300px" required="required" /></td>
        </tr>
        <tr>
            <td>Zubereitungszeit:</td>
            <td><input type="number" name="duration" value="<% $.duration %>" placeholder="(in Minuten)" style="width: 300px" required="required" min="0" step="1" /></td>
        </tr>
	    <tr>
    		<td>Kategorie:</td>
			<td>
				<select name="category" style="width: 200px">
% foreach my $category (@categories) {
	                <option required="required"
	                    value=<% $category->{id} %>
% if ($.category eq $category->{id}) {
                        selected="selected"
% }
	                    ><% $category->{name} %></option>
% }
				</select>
			</td>		
	    </tr>
	    <tr>
		    <td>Schwierigkeitslevel:</td>
		    <td>
			    <select name="difficulty" style="width: 200px" >
% foreach my $difficulty (@difficulties) {
	                <option required="required"
	                    value=<% $difficulty->{id} %>
% if ($.difficulty eq $difficulty->{id}) {
                        selected="selected"
% }
	                ><% $difficulty->{description} %></option>
% }
			    </select>
		    </td>
	    </tr>
	    <tr>
		<td align="left" colspan="2">
			<textarea name="textarea" id="textarea"><% $.textarea %></textarea>
			<script>
				// Replace the <textarea id="content"> with a CKEditor
				// instance, using default configuration.
				CKEDITOR.replace('textarea',{
					width   : '645px',
					height  : '300px'
				});
			</script>
			<br />
		</td>
	    </tr>
	    <tr>
		    <td align="left" colspan="2">
		        <span>
                    <input style="float:left" type="submit" name="Save" class="btn btn-success" value="Speichern" /> &nbsp;
                    <input type="submit" name="Delete" class="btn btn-danger" value="Löschen" />
                </span>
                <br />
                <br />
    		</td>
	    </tr>
    </table>
</form>
</div>