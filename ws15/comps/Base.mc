<%class>
has 'maintitle' => (default => 'WAE Group 5');
</%class>

<%augment wrap
<!DOCTYPE html>
  <html>
    <head>
      <meta charset="UTF-8" http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width = device-width, initial-scale = 1">
      <link rel="stylesheet" type="text/css" href="static/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="static/css/dropdown.css">
      <link rel="stylesheet" type="text/css" href="static/css/style.css">
      <script src="static/js/jquery.min.js"></script>
      <script src="static/js/bootstrap.min.js"></script>
% $.Defer {{
      <title><% $.maintitle %></title>
% }}
      <% $.head %>
    </head>
    <body>
      <div id="header">
        <& template/header.mi &>
      </div>
      <div id="content">
        <% inner() %>
      </div>
      <div id="message" style="margin-top: 10px;">
        <& template/message.mi, message => delete($m->session->{message}) &>
      </div>
      <div id="footer">
        <& template/footer.mi, grp => '5', sem => 'WS2015' &>
      </div>
    </body>
  </html>
</%augment>

<%flags>
extends => undef
</%flags>
