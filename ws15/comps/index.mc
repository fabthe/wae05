<%method head>
</%method>
<%init>

    my $username = $m->session->{'username'};
    if ($m->request_path eq "/wae05") {
        $m->redirect("/wae05/");
    }
</%init>

<div class="container">
	<div class="jumbotron">		
		<h3>Herzlich Willkommen <% $username %>!</h3>
		<p>
		    Viel Spaß beim Stöbern und gutes Gelingen<br />
		    wünscht Ihnen das WAE Team 5!
		</p>
		<img src="static/images/chef.png" width="80" height="80"></img>
	</div>

    <% # Show all recipes %>
    <& template/recipes.mi &>
</div>