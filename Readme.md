## wae05
* Web Application Engineering & Content Management (VU)
* Group 05
* http://weng.culturall.com:8020/AngabeWAE (assignment)
* http://weng.culturall.com:8020/wae05/

## Setup local development environment
### Installation
* Install perl5 (e.g. `apt-get install perl`)
* Install mysql (e.g. `apt-get install mysql-server`)
* Install cpan. When running cpan the first time and you are prompted with an installation/setup dialog, use default settings.
* Install cpanm (cpan-minus)
    * `curl -L http://cpanmin.us | perl - --sudo App::cpanminus`
* Install modules using cpanm
    * `cpanm -S --notest Poet MooseX::Getopt DBIx::Connector DBD::mysql`
    * In case of errors (something with --mysql_config missing) run: `apt-get install libmysqlclient-dev`

### Setup database
* Make sure mysql is installed properly, e.g. `mysql --version`
* Login as root
    * `shell> mysql --user=root mysql`
* Create user 'tuv' with all privileges
    * `mysql> CREATE USER 'tuv'@'localhost' IDENTIFIED BY 'tuv14';`
    * `mysql> GRANT ALL PRIVILEGES ON *.* TO 'tuv'@'localhost'
    ->     WITH GRANT OPTION;`
    * `Ctrl-C`
* Login as 'tuv'
    * `shell> mysql --user=tuv mysql -p`
* Create database
    * `mysql> create DATABASE IF NOT EXISTS tuv;`
* Import schema into database (export from weng.culturall by using phpMyAdmin)
    * `shell> mysql -u tuv -p tuv < ws15/db/schema.sql`
* Import data into database (export from weng.culturall by using phpMyAdmin)
    * `shell> mysql -u tuv -p tuv < ws15/db/data.sql`

### Run the application
* `./ws15/bin/run.pl` will start the application
* Visit http://localhost::9001/

## Deploy
* Establish a SFTP connection to weng.culturall.com, e.g. by using FileZilla
* Copy content of local `/ws15/comps` folder into `/usr/local/htdocs/ws15/comps/wae05` on the remote site
* Visit http://weng.culturall.com:8020/wae05/

## Mockup
![Mockup][1]

## Reality Check
![RealityCheck][2]

[1]: Mockup.jpeg
[2]: RealityCheck.png